# Ternary

x = 1
y = 0

if x > 0:
    y = 2
else:
    y = -2

y = 2 if x > 0 else -2

print("y : " + str(y))  # int() float()
