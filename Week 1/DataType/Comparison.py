# ( < ) kurang dari untuk String

print('abce' < 'bbcd')
print('A' < 'a')    # ASCII a non kapital lebih besar daripada A kapital
                    # ASCII representasi angka dari suatu karakter (bisa huruf, tanda baca, dll)

# ( == ) dan ( != )
print('a' == 'a')
print(3.0 > 2)