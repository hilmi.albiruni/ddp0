# Operasi untuk angka

x1 = 4 * 3
x2 = 5 + 3.0
x3 = 6 / 3.0  # hasilnya pasti float
x4 = 10 % 2.0
x5 = 20 // 9  
x6 = 3**3

# untuk operasi angka jika salah satu float maka hasilnya pasti float
# float = ada koma, 2.0 itu float

print(x1) # 12.0
print(x2) # 8.0
print(x3) # 2.0
print(x4) # 0.0
print(x5) # 2
print(x6) # 27

# shorthand (bakal sering dipake)

y1 = 2
y1 += 2  # setara dengan y1 = y1 + 2
print(y1)

y2 = 3
y2 /= 2
print(y2)

# berlaku juga untuk -= *= //= %= **=


# Operasi + dan * tapi pada String

z1 = "abc"
z1 += "d"
print("z1 : " + z1)

z1 *= 2
print("z1 : " + z1)


x1 = 2
x1 += 2
print(x1)