a = 100  # Integer

b = 20.0  # Float

c = 'halo' # String

d = "halo" # String

e = """
Halo semua,
Apa kabar?,
kokoaksd
sadad
asdaasda
"""        # String multi line

f = True  # Boolean

g = False  # Boolean

h = [1,1,2,'3',True]  # List

i = (1,3,'3', 6)  # Tuple

j = {1,1,2, '5', 'ok'}  # Set

k = {'nama': 'Gani', 'umur': 19}  # Dictionary

h.append('gani')
j.add('o')
k['alamat'] = "Jepara"

print("aku \nkamu")

nama_lengkap = "Gani Ilham Irsyadi"
nama_lengkap = 20

print(nama_lengkap)
print(h)
print(j)
print(k['nama'])