# kata = "abcdefghijk"
# list1 = [1,2,3,4]

# for o in kata:
#     print("(" + o + ")", end=" ")

# for o in list1:
#     print(o, end="")

# variabel = 200
# for i in variabel:
#     print(i)


# if else
# tipe data
# operation, comparison
# for _ in _

kata = "gasi"  # g4s1

hasil = ""

for i in kata:
    if i == 'a':
        hasil += "4"
    elif i == 'i':
        hasil += "1"
    else:
        hasil += i
else:
    print("selesai")

print(hasil)