while True:
    x = int(input("Masukkan angka kurang dari 5 : "))
    if x < 5:
        break

for i in range(10):
    print(i)
    if i == 3:
        break

print("selesai")

# fungsinya untuk keluar dari loop

print('='*50)
    
for i in range(8):
    if i == 3:
        continue
    # statement setelah continue tidak dieksekusi
    print(i, end=" ")
    print('halo', end=" ")

print()  # ngasih enter

for i in range(3):
    if i == 2:
        pass
    print(i)