nama = "Steven"
print("Hello " + nama + "!")
# Output : Hello Steven!
# Harus meng-hardcode nama dengan kode, tidak user-friendly

print('-'*50)

# nama = input("Siapa nama anda? \nJawaban: ")
# print("Hello " + nama + "!")

# Parameter "Teks pertanyaan" akan selalu dicetak disamping input
# Program akan berhenti sementara setelah mencetak "Siapa nama anda? "
# sampai user mengetik namanya dan menekan ENTER
# setelah tombol ENTER ditekan maka program akan menyambut user dengan namanya.

print('-'*50)

# nama = input("Siapa nama anda? ")
# status_mahasiswa = input("Apakah anda seorang mahasiswa? ")
# if status_mahasiswa.lower() == "iya":
#     # why?
#     # lower() untuk lowercase String
#     # upper() untuk uppercase String
# 	  univ = input("Dimana anda sedang berkuliah? ")
# 	  print("Nama anda " + nama + " yang berkuliah di " + univ)
# else:
# 	  print("Nama anda " + nama + " yang bukan seorang mahasiswa")

# type() untuk cek tipe object

print('-'*50)

string_a = "dia berkata 'halo!' "
print(string_a)

string_b = 'dia berkata "halo" '
print(string_b)
 