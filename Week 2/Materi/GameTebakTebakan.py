import random
import math

jawaban = random.randint(1,10) # random.randint(1,10) berarti angka random dari 1 sampai 10
print(jawaban)
percobaan = 0

# Selama jumlah percobaan dibawah 3 maka lakukan kode dibawah
while percobaan < 3:
		jawab_user = input("Tebak angka 1 - 10: ") # Minta jawaban user
		if int(jawab_user) != jawaban:
				# Bila jawaban salah maka lanjut ke loop selanjutnya setelah percobaan += 1
				# Sehingga akan bertanya ke user melalui input() di atas ini.
				print("Jawaban salah!")
		else:
				# Bila jawaban benar maka beri tahu user, dan break loop
				print("Jawaban benar! yaitu: " + str(jawaban))
				break
		# Tambah ke counter percobaan
		percobaan += 1
else:
		# Ingat else untuk while HANYA berjalan jika while loop berhenti bukan dari break statement
		print("Sayang sekali, namun jawabannya adalah " + str(jawaban))
