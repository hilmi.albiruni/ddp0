print("Selamat datang di kalkulator IPK!")
# inisialisasi variabel menyimpan jumlah kedua buah sisi pembagian
jumlah_sks = 0
jumlah_nilai_kali_sks = 0

# Berarti akan berjalan selamanya hingga di-break
while True:
		# float(input()) akan langsung mengubah input user ke sebuah floating point
		nilai = float(input("Masukkan nilai mata kuliah anda: "))
		sks = int(input("Masukkan jumlah sks mata kuliah tersebut: "))
		
		# Ingat kembali arti dari operator +=
		jumlah_sks += sks
		jumlah_nilai_kali_sks += nilai*sks
		# Tanya apakah ingin lanjut, bila "T" maka hentikan while loop dan hitung IPK
		lanjut = input("Apakah masih ingin lanjut? bila tidak masukkan T: ")
		if lanjut == "T":
				break
		
		# Bila tidak masuk ke if untuk break, maka while loop terus berulang

print("IPK anda adalah " + str(jumlah_nilai_kali_sks/jumlah_sks))