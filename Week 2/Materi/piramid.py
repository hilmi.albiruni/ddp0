"""
*
**
***
****
"""

# 4 baris
for i in range(1,5):
    print('*'*i)

print("*"*5)

"""
****
***
**
*
"""

# 4 baris
for i in range(4,0,-1):
    print("*"*i)


"""

4 baris

   *     1  (spasi 3x + bintang 1x + \n)
  ***    2  (spasi 2x + bintang 3x + \n)
 *****   3  (spasi 1x + bintang 5x + \n)
*******  4  (spasi 0x + bintang 7x + \n)
"""

# 4 baris

baris = 8
jumlah_spasi = baris - 1
jumlah_bintang = 1
for i in range(baris):
    print(" "*(jumlah_spasi), end="")
    print("*"*jumlah_bintang, end="")
    print()
    jumlah_bintang += 2
    jumlah_spasi -= 1