x = 1
y = 1
for z in range(5):
    print("x before : " + str(x))
    print("y before : " + str(y))
    temp = x
    x = y
    y += temp
    print("x after : " + str(x))
    print("y after : " + str(y))
else:
    print("Nilai x adalah ", x)
    print("Nilai y adalah ", y)