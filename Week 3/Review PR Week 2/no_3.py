a = input()
b = input()
c = input()

if a:  # 
	print("a")
elif int(b):
	print("b")
elif int(c) and int(c) > 10:
	print("c")

# Apabila nilai a BUKAN merupakan string kosong maka mencetak `a`
	# string kosong itu nilai booleannya false
# Apabila nilai b BUKAN 0 maka mencetak `b` (Hanya terjadi jika kondisi a tidak terpenuhi)
	# integer 0 itu nilai booleannya false
# Apabila nilai c BUKAN 0 dan nilai c lebih dari 10 (eksklusif) maka print `c`  (Hanya terjadi jika kondisi a dan b keduanya tidak terpenuhi)
	# (statement) and (value)
	# kalo statement True hasilnya value
	# kalo statement False hasilnya False