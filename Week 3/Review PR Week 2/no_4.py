def angka_penting(angka):
    if angka % 1 > 0.5:
        return int(angka) + 1
    elif angka % 1 < 0.5:
        return int(angka)
    else:
        if int(angka) % 2 == 0:
            return int(angka)
        else:
            return int(angka) + 1

print(angka_penting(75.7))
print(angka_penting(42.3))
print(angka_penting(42.5))
print(angka_penting(43.5))