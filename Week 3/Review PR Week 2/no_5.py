inp = int(input("Input ukuran piramida: "))

if inp >= 0:
    for i in range(1, inp + 1):
        print(" "*(inp-i) + "*"*i, end="")
        print('*'*(i-1))
else:
    print("Input yang dimasukkan bukan bilangan bulat non-negatif!")
