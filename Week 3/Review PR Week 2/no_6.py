# Program akan jalan terus sampai user meminta keluar.
while True:
    angka = input("Silakan masukan bilangan dan basisnya yang dipisahkan tanda (_): ")
    
    # Keluar program.
    if angka == "EXIT":
        print("Terima kasih telah menggunakan program pencari nilai desimal!")
        # Keluar dari while-loop.
        break

    # Pisahkan kedua angka kemudian cek formatnya.
    angka = angka.split("_")
    if len(angka) == 2:
        # Angka yang dipisah dimasukkan ke variabel.
        dicari = angka[0]
        basis = int(angka[1])

        # Cek basisnya.
        if 2 <= basis <= 10 or basis == 16:
            # Bikin variabel yang kosong, kemudian isi dengan nama basis.
            nama_basis = None
            if (basis == 2):
                nama_basis = "biner"
            elif (basis == 8):
                nama_basis = "oktal"
            elif (basis == 10):
                nama_basis = "desimal"
            elif (basis == 16):
                nama_basis = "heksadesimal"
            else:
                nama_basis = "basis-" + str(basis)
            
            try:
                # Ubah angka dan print hasil.
                desimal = int(dicari, basis)
                print("Nilai desimal dari bilangan " + str(nama_basis) + " " + dicari + " adalah " + str(desimal) + ".")
            except:
                # Kalau bukan bilangan bulat, print sebuah peringatan.
                print("Tolong gunakan bilangan bulat!")

        else:
            # Kalau basisnya tidak sesuai, print sebuah peringatan.
            print("Tolong gunakan basis 2 sampai 10 atau basis 16!")
    else:
        # Kalau formatnya tidak sesuai, print sebuah peringatan
        print("Tolong gunakan format yang sesuai (xxxxx_n)!")
        
    # Setiap kali program selesai berjalan, print lline baru agar sesuai permintaan soal
    print()