# contoh implementasi dengan asumsi 2 <= b <= 16
n = input()  # angka nya
a = input()  # basis awal
b = int(input())  # basis tujuan

# angka dalam basis awal diubah ke desimal (basis 10)
x = int(n, base=int(a))  # hasilnya/ returnnya base-10

# basis 8 -> basis 10
# 12
# 1*(8**1) + 2*8(**0)
# 10

# basis 10 -> basis 16
# 23
# 23 modulo 16 = 7
# 23 // 16 = 1

# basis 10 -> basis 8
# 100
# 410?

# 100 modulo 8 = 4
# 100 // 8 = 12

# 12 modulo 8 = 4
# 12 // 8 = 1

# 1 modulo 8 = 1
# 1 // 8 = 0   (berhenti kalo udah 0)

# disusun dari bawah ke atas


# 144
# 1*(8^2) + 4*(8^1) + 4*(8^0)
# 64 + 32 + 4
# 100

ans = ''
while (x > 0):
    c = x % b
    x //= b
    if (0 <= c <= 9):
        tmp = str(c)
    elif (10 <= c <= 15):
        tmp = chr(97+(c-10))
    ans = tmp + ans
    #  1: ans = tmp + ans(ans yg sebelumnya) = '4' + ''(ans yg sebelumnya)
    #  2: ans = tmp + ans(ans yg sebelumnya) =  '4' + '4'(ans yg sebelumnya) = '44'
    #  3: ans = '1' + '44' = '144'

print(ans)
