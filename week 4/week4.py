# List and multidimension list

mtx = [[0,1,2], [3,4,5], [6,7,8]]

for i in mtx:
    for j in i:
        print(j, end=' ')
    print()

# Comparison

print("H" > "h")

# 0 1 2
# 3 4 5
# 6 7 8

# 0 3 6
# 1 4 7
# 2 5 8

# Append
lst = [1,2,3]
print(lst)
lst.append("halo")
print(lst)

# Extend
lst = [1,2,3]
lst.extend([1,2,3])
print(lst)
lst.append(3)
print(lst)

# Append vs Extend
lst = [1,2,3]
lst.extend("Halo")
print(lst)

# Insert
lst = [1,2,3]
lst.insert(100, 4)
print(lst)

# Remove
lst = [2,3,4,3]
lst.remove(3)
print(lst)

# Pop
lst = [1,2,3]
print(lst.pop(1))
print(lst)

# Index
lst = [1,2,3,2]
print(lst.index(2))

# Split dan Join
lst = ['Se', 'la', 'mat']
sep = ','
print(sep.join(lst))

barang = "baju celana tas"
print(barang.split(" "))

# Sorted
lst = [2,1,5]
print(sorted(lst))
print(lst.sort())
print(lst)

# Mutability
lst = [1,2,3]
lst[2] = 4
print(lst)

# ini error
s = 'haha'
s[0] = 'a'

# shallow copying
a = [1,2,3,[1,2]]
b = a.copy()
b[3].append(4)
print(a)
print(b)

# deep copying
from copy import deepcopy
a = [1,2,3]
b = deepcopy(a)

a.append(4)
print(a)
print(b)

# Set
a = set('abcda')
b = set('cdef')

print(b.difference(a))

# Dictionary
biodata = {}
ttgl = {}
biodata['nama'] = 'samuel'
biodata['tempat_tinggal'] = ttgl

ttgl['alamat'] = 'Jakarta Timur'
ttgl['kota'] = 'Jakarta'

biodata['lahir'] = {"tempat": "Jakarta", "tanggal": "1-1-2001"}
biodata['lahir']['test'] = 'test'
print(biodata)

print(biodata['lahir']['tempat'])

for key in biodata:
    print(biodata[key])
